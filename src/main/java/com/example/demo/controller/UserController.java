package com.example.demo.controller;

import com.example.demo.dto.OrderDTO;
import com.example.demo.dto.UserDTO;


import com.example.demo.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;


    //get all user-service data
    @GetMapping("/getAll")
    public List<UserDTO> getAllUser(){

        return userService.getAllUsers();
    }

    //get order-service record data using id
    @GetMapping("/getOrderByUserId/{id}")
    public List<OrderDTO>getOrderByUserId(@PathVariable final Long id){
        return userService.getOrderByUserId(id);

    }

    //create new user
    @PostMapping("/createUser")
    public boolean createUser(@RequestBody UserDTO user){
      return   userService.createUser(user);

    }

    //update user service record
    @PutMapping("/updateUser/{id}")
    public boolean updateUser(@RequestBody UserDTO user,@PathVariable final Long id){
        return userService.updateUser(user,id);

    }








}
