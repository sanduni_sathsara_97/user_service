package com.example.demo.service;

import com.example.demo.dto.OrderDTO;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.UserEntity;
import com.example.demo.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private UserRepository repository;

    @Autowired
    private RestTemplateBuilder restTemplate;

    //get All user data
    public List<UserDTO> getAllUsers(){
        List<UserDTO>users=null;
        try {
            users = repository.findAll()
                    .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());

        }catch(Exception ex){
            LOGGER.warn("Exception in user service"+ex);
        }
        return users;
    }

    public List<OrderDTO>getOrderByUserId(Long id){
        List<OrderDTO>orders=restTemplate.build().getForObject(
                orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/"+id),
                List.class);
        return orders;
    }

    public boolean createUser(UserDTO user){
        try {
            UserEntity userEntity=new UserEntity();
            userEntity.setId(user.getId());
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            repository.save(userEntity);
            return true;

        }catch (Exception ex){
          LOGGER.warn("User service error"+ex);
        }
        return false;
    }

    public boolean updateUser(UserDTO user,Long id){
        try {
            UserEntity userEntity=new UserEntity();
            userEntity.setName(user.getName());
            userEntity.setAge(user.getAge());
            userEntity.setId(id);
            repository.save(userEntity);
            return true;

        }catch (Exception ex){
            LOGGER.warn("User service error:"+ex);

        }
        return false;

    }



}
